import { combineReducers, Reducer } from 'redux';
import { connectRouter, RouterState, LocationChangeAction } from 'connected-react-router';
import { CounterState, counterReducer } from './counterReducer';

export interface RootState {
    counter: CounterState;
}

export const rootReducer = combineReducers<RootState | undefined>({
    counter: counterReducer
});
