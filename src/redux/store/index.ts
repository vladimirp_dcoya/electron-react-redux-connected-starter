import { createStore, applyMiddleware, compose, Store } from 'redux';
import {
    forwardToMain,
    forwardToRenderer,
    triggerAlias,
    replayActionMain,
    replayActionRenderer
} from 'electron-redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import { rootReducer, RootState } from '../reducers';

export default function configureStore(
    initialState?: RootState,
    scope = 'main'
): Store<RootState | undefined> {
    let middleware = [];

    if (!process.env.NODE_ENV) {
        // middleware.push(logger);
    }

    if (scope === 'renderer') {
        middleware = [forwardToMain, ...middleware];
    }

    if (scope === 'main') {
        middleware = [triggerAlias, ...middleware, forwardToRenderer];
    }

    const enhancer = composeWithDevTools(applyMiddleware(...middleware));

    const store = createStore(rootReducer, initialState, enhancer);

    if (!process.env.NODE_ENV && module.hot) {
        module.hot.accept('../reducers', () => {
            store.replaceReducer(require('../reducers'));
        });
    }

    if (scope === 'main') {
        replayActionMain(store);
    } else {
        replayActionRenderer(store);
    }

    return store;
}
