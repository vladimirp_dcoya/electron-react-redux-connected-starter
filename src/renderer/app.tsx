/* eslint-disable func-names */
/* eslint-disable react/jsx-closing-bracket-location */
/* eslint-disable @typescript-eslint/ban-ts-ignore */
import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { AppContainer } from 'react-hot-loader';
import styled from 'styled-components';
import { Route, Switch, Router } from 'react-router'; // react-router v4/v5
import { remote } from 'electron';
import { createBrowserHistory } from 'history';
import * as electronRedux from 'electron-redux';
import ChatbotWindow from './components/ChatbotWindow';
import Application from './components/Application';
import configureStore from '../redux/store';
import { initialState } from '../main/main';

// Create main element
const mainElement = document.createElement('div');
document.body.appendChild(mainElement);

const renderAssistantSidebar = () => {
    const Container = styled.div`
        opacity: 100%;
    `;

    return (
        <Container>
            <Application />
        </Container>
    );
};

const Dashboard = () => <div>This is a dashboard</div>;

const Notification = () => (
    <div>
        <div>{remote.getCurrentWebContents().getURL()}</div>
        <div>{renderAssistantSidebar()}</div>
    </div>
);

const store = configureStore(electronRedux.getInitialStateRenderer(), 'renderer');

// Render components
const render = () => {
    const history = createBrowserHistory();
    ReactDOM.render(
        <AppContainer>
            <Provider store={store}>
                <Router history={history}>
                    <Switch>
                        <Route exact path="/dashboard" component={Dashboard} />
                        <Route exact path="/chatbot" component={ChatbotWindow} />
                        <Route path="/notifications" component={Notification} />
                        <Route path="/" component={renderAssistantSidebar} />
                    </Switch>
                </Router>
            </Provider>
        </AppContainer>,
        mainElement
    );
};

render();
