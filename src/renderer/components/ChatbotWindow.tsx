import * as React from 'react';
import { remote } from 'electron';

const openDashboard = () => {
    const { BrowserWindow } = remote;
    const win = new BrowserWindow({
        height: 600,
        width: 800
    });

    win.loadURL('http://localhost:2003/dashboard');
};

const closeCurrentWindow = () => {
    remote.getCurrentWindow().close();
};

const maximize = () => {
    remote.getCurrentWindow().setSize(1200, 800, true);
};

const minimize = () => {
    remote.getCurrentWindow().setSize(600, 800, true);
};

const openNotification = () => {
    const { BrowserWindow } = remote;
    const win = new BrowserWindow({
        height: 200,
        width: 400
    });

    win.loadURL(`http://localhost:2003/notifications?id=32545646`);
};

const ChatbotWindow = () => {
    return (
        <div>
            <div>this is a chatbot</div>
            <button onClick={closeCurrentWindow}>close current window</button>
            <button onClick={openDashboard}>open dashboard</button>
            <button onClick={maximize}>maximize dashboard</button>
            <button onClick={minimize}>minimize dashboard</button>
            <button onClick={openNotification}>open notification</button>
        </div>
    );
};

export default ChatbotWindow;
