// in the main store
import * as electronRedux from 'electron-redux';
import { app, BrowserWindow } from 'electron';
import * as path from 'path';
import * as url from 'url';
import installExtension, {
    REACT_DEVELOPER_TOOLS,
    REDUX_DEVTOOLS
} from 'electron-devtools-installer';
import { applyMiddleware, createStore, Store, AnyAction } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import { createBrowserHistory } from 'history';
import { forwardToRenderer } from 'electron-redux';
import { rootReducer, RootState } from '../redux/reducers';
import configureStore from '../redux/store/index';

let win: BrowserWindow | null;
const installExtensions = async () => {
    return Promise.all([installExtension(REACT_DEVELOPER_TOOLS), installExtension(REDUX_DEVTOOLS)]);
};

export const initialState: RootState = { counter: { value: 0 } };

const createWindow = async () => {
    const store = configureStore(initialState, 'main');

    // store.subscribe(async () => {
    //     // persist store changes
    //     // TODO: should this be blocking / wait? _.throttle?
    //     await storage.set('state', store.getState());
    // });

    if (process.env.NODE_ENV !== 'production') {
        await installExtensions();
    }

    // transparent = true,
    // resizable = false,
    // frame = false,
    // skipTaskbar = true
    win = new BrowserWindow({ width: 800, height: 600 });

    if (process.env.NODE_ENV !== 'production') {
        process.env.ELECTRON_DISABLE_SECURITY_WARNINGS = '1'; // eslint-disable-line require-atomic-updates
        win.loadURL(`http://localhost:2003/`);
    } else {
        win.loadURL(
            url.format({
                pathname: path.join(__dirname, 'index.html/'),
                protocol: 'file:',
                slashes: true
            })
        );
    }

    if (process.env.NODE_ENV !== 'production') {
        // Open DevTools, see https://github.com/electron/electron/issues/12438 for why we wait for dom-ready
        win.webContents.once('dom-ready', () => {
            win!.webContents.openDevTools();
        });
    }

    win.on('closed', () => {
        win = null;
    });
};

app.on('ready', createWindow);

app.on('window-all-closed', () => {
    if (process.platform !== 'darwin') {
        app.quit();
    }
});

app.on('activate', () => {
    if (win === null) {
        createWindow();
    }
});
