/* eslint-disable react/jsx-closing-bracket-location */
import { hot } from 'react-hot-loader/root';
import * as React from 'react';
import { remote } from 'electron';

import CounterContainer from '../containers/CounterContainer';

const openChatbot = () => {
    const { BrowserWindow } = remote;
    const win = new BrowserWindow({
        height: 600,
        width: 800
    });

    win.loadURL('http://localhost:2003/chatbot');
};

const Application = () => (
    <div>
        <button onClick={openChatbot}>chatbot</button>
        <CounterContainer />
    </div>
);

export default hot(Application);
